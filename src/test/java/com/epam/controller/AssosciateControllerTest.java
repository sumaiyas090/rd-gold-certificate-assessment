package com.epam.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.dtos.AssociateDto;
import com.epam.dtos.BatchDto;
import com.epam.service.AssociateService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;



@WebMvcTest(AssociateController.class)
public class AssosciateControllerTest {
	@MockBean
	AssociateService associateService;
	@Autowired
	MockMvc mockMvc;
	AssociateDto associateDto;
	BatchDto batchDto;
	AssociateDto dummy;
	@BeforeEach
	void setup() {
		associateDto=new AssociateDto();
		associateDto.setId(1);
		associateDto.setName("sumaiya");
		associateDto.setCollege("vid");
		associateDto.setEmail("Email@gmailcom");
		associateDto.setGender("female");
		associateDto.setStatus("active");
		
		dummy=new AssociateDto();
		
		batchDto=new BatchDto();
		batchDto.setId(1);
		batchDto.setName("JavaRD2022");
		batchDto.setPractice("java");
		batchDto.setEndDate(Date.from(Instant.now()));
		batchDto.setStartDate(Date.from(Instant.now()));
		
	}
	
	@Test
	void testPostMapping() throws JsonProcessingException, Exception {
		Mockito.when(associateService.addAssociate(associateDto)).thenReturn(associateDto);
    	mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(associateDto)))
    	.andExpect(status().isCreated())
    	.andReturn();
	}
	@Test
	void testGetMapping() throws Exception {
		Mockito.when(associateService.getAssociatesByGender("female")).thenReturn(List.of(associateDto));
		mockMvc.perform(get("/rd/associates/{gender}","female")).andExpect(status().isOk()).andReturn();
	}
	@Test
	void testDeleteMapping() throws Exception {
		Mockito.doNothing().when(associateService).deleteAssociate(1);;
    	mockMvc.perform(delete("/rd/associates/1"))
    	.andExpect(status().isNoContent())
    	.andReturn();
	}
	@Test
	void testPutMaaping() throws JsonProcessingException, Exception {
		Mockito.when(associateService.modifyAssociate(associateDto)).thenReturn(associateDto);
    	mockMvc.perform(put("/rd/associates").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(associateDto)))
    	.andExpect(status().isOk())
    	.andReturn();
	}
	 @Test
	    void testMethodNotValid() throws JsonProcessingException, Exception {
	    	Mockito.when(associateService.addAssociate(dummy)).thenReturn(dummy);
	    	mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(dummy)))
	    	.andExpect(status().isBadRequest())
	    	.andReturn();
	    	
	}
	 @Test
	 void testhttpNotReadble() throws JsonProcessingException, Exception {
	    	Mockito.when(associateService.addAssociate(dummy)).thenReturn(dummy);
	    	mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString("{")))
	    	.andExpect(status().isBadRequest())
	    	.andReturn();
	    }
	  

}
