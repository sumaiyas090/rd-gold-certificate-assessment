package com.epam.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.dtos.AssociateDto;
import com.epam.dtos.BatchDto;
import com.epam.entities.Associate;
import com.epam.entities.Batch;
import com.epam.exception.AssociateException;
import com.epam.repository.AssociateRepo;
import com.epam.repository.BatchRepo;

@ExtendWith(MockitoExtension.class)
public class AssociateServiceTest {
	@Mock
	BatchRepo batchRepo;
	@Mock
    AssociateRepo associateRepo;
	@Mock
	ModelMapper mapper;
	@InjectMocks
	AssociateServiceImpl associateService;
	AssociateDto associateDto;
	BatchDto batchDto;
	Batch batch;
	Associate associate;
	
	@BeforeEach
	void setup() {
		batchDto=new BatchDto();
		batchDto.setId(1);
		batchDto.setName("JavaRD2022");
		batchDto.setPractice("java");
		batchDto.setEndDate(Date.from(Instant.now()));
		batchDto.setStartDate(Date.from(Instant.now()));
		
		batch=new Batch();
		batch.setId(1);
		batch.setName("JavaRD2022");
		batch.setPractice("java");
		batch.setEndDate(Date.from(Instant.now()));
		batch.setStartDate(Date.from(Instant.now()));
		associateDto=new AssociateDto();
		
		associateDto.setId(1);
		associateDto.setName("sumaiya");
		associateDto.setCollege("vid");
		associateDto.setEmail("Email@gmailcom");
		associateDto.setGender("female");
		associateDto.setStatus("active");
		associateDto.setBatch(batchDto);
		
		associate=new Associate();
		associate.setId(1);
		associate.setName("sumaiya");
		associate.setCollege("vid");
		associate.setEmail("Email@gmailcom");
		associate.setGender("female");
		associate.setStatus("active");
		associate.setBatch(batch);
		
	}
	@Test
	void testAddAssociate() {
		Mockito.when(mapper.map(associateDto, Associate.class)).thenReturn(associate);
		Mockito.when(batchRepo.findById(1)).thenReturn(Optional.ofNullable(batch));
		Mockito.when(associateRepo.save(associate)).thenReturn(associate);
		Mockito.when(mapper.map(associate,AssociateDto.class)).thenReturn(associateDto);
		assertEquals(associateDto, associateService.addAssociate(associateDto));
	}
	@Test
	void testGetAssociates() {
		Mockito.when(associateRepo.findAllByGender("female")).thenReturn(List.of(associate));
		Mockito.when(mapper.map(associate,AssociateDto.class)).thenReturn(associateDto);
		assertEquals(List.of(associateDto),associateService.getAssociatesByGender("female"));
	}
	@Test
	void testModifyAssociates() {
		Mockito.when(associateRepo.existsById(1)).thenReturn(true);
    	Mockito.when(mapper.map(associateDto, Associate.class)).thenReturn(associate);
		Mockito.when(associateRepo.save(associate)).thenReturn(associate);
		Mockito.when(mapper.map(associate, AssociateDto.class)).thenReturn(associateDto);
		assertEquals(associateDto,associateService.modifyAssociate(associateDto));
	}
   @Test
   void testModifyAssociateWithException() {
	   Mockito.when(associateRepo.existsById(1)).thenReturn(false);
   	   assertThrows(AssociateException.class,()->associateService.modifyAssociate(associateDto));
   }
   @Test
   void testDeleteAssociate() {
	   Mockito.doNothing().when(associateRepo).deleteById(1);
   	   associateService.deleteAssociate(1);
   }
   @Test
  void testBatchGetters() {
	  assertEquals(batchDto.getId(), 1);
	  assertEquals(batchDto.getName(),"JavaRD2022");
	  assertEquals(batchDto.getPractice(), "java");
	  assertNotNull(batchDto.getStartDate());
	  assertNotNull(batchDto.getEndDate());
	  
	  assertEquals(batch.getId(), 1);
	  assertEquals(batch.getName(),"JavaRD2022");
	  assertEquals(batch.getPractice(), "java");
	  assertNotNull(batch.getStartDate());
	  assertNotNull(batch.getEndDate());  
	  
  }
   
   @Test
   void testAssociateGetters() {
	   assertEquals(associateDto.getId(),1);
	   assertEquals(associateDto.getName(), "sumaiya");
	   assertEquals(associateDto.getCollege(), "vid");
	   assertEquals(associateDto.getEmail(),"Email@gmailcom");
	   assertEquals(associateDto.getGender(), "female");
	   assertEquals(associateDto.getStatus(), "active");
	   assertEquals(associateDto.getBatch(),batchDto);
	   
	   assertEquals(associate.getId(),1);
	   assertEquals(associate.getName(), "sumaiya");
	   assertEquals(associate.getCollege(), "vid");
	   assertEquals(associate.getEmail(),"Email@gmailcom");
	   assertEquals(associate.getGender(), "female");
	   assertEquals(associate.getStatus(), "active");
	   assertEquals(associate.getBatch(),batch);
   }
}
