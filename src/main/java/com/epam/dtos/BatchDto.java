package com.epam.dtos;

import java.util.Date;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class BatchDto {
	@Schema(accessMode =AccessMode.READ_ONLY)
	private int id;
	@NotBlank(message = "batch name should not be blank")
	private String name;
	@NotBlank(message = "batch practice should not be blank")
	private String practice;
	@NotBlank(message = "start date should not be blank")
	private Date startDate;
	@NotBlank(message = "end date should not be blank")
	private Date endDate;
	
}
