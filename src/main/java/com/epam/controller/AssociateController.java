package com.epam.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dtos.AssociateDto;
import com.epam.service.AssociateService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/rd/associates")
@Slf4j
public class AssociateController {
	@Autowired
	private AssociateService associateService;
	@PostMapping
	public ResponseEntity<AssociateDto> addAssociate(@RequestBody @Valid AssociateDto associateDto){
		log.info("Received request for post mapping");
		return new ResponseEntity<>(associateService.addAssociate(associateDto), HttpStatus.CREATED);
		
	}
	@GetMapping("/{gender}")
	public ResponseEntity<List<AssociateDto>> getAssociatesByGender(@PathVariable String gender){
		log.info("Received request for get mapping");
		return new ResponseEntity<>(associateService.getAssociatesByGender(gender), HttpStatus.OK);
		
	}
	@PutMapping
	public ResponseEntity<AssociateDto> modifyAssociate(@RequestBody @Valid AssociateDto associateDto){
		log.info("Received request for put mapping");
		return new ResponseEntity<>(associateService.modifyAssociate(associateDto), HttpStatus.OK);
		
	}
	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteAssociate(@PathVariable int id) {
		log.info("Received request for delete mapping");
		associateService.deleteAssociate(id);
	}

}
